

Zepto(function($) {
    function preloadimages(arr) {
		var newimages = [],
			loadedimages = 0;
		var postaction = function() {};
		var arr = (typeof arr != "object") ? [arr] : arr;

		function imageloadpost() {
			loadedimages++;
			if (loadedimages == arr.length) {
				postaction(newimages)
			}
		}
		for (var i = 0; i < arr.length; i++) {
			newimages[i] = new Image();
			newimages[i].src = arr[i];
			newimages[i].onload = function() {
				imageloadpost()
			};
			newimages[i].onerror = function() {
				imageloadpost()
			}
		}
		return {
			done: function(f) {
				postaction = f || postaction
			}
		}
	}

	preloadimages(['images/bg1.jpg','images/home-page-main.png','images/mascot.png',
		'images/theme.png','images/guide-main.png','images/video-detail.png', 'css/images/note.png','images/suning-cloud-intro.png','images/suning-intro.png'
	]).done(function() {
		$('#loading').addClass('hide');
		$('.page0').addClass('cur showed')
		$('.shou').show();
		pw.freeze(false);

	});

	var pw = new pageSwitch('pages', {
		duration: 600,
		start: 0,
		direction: 1,
		loop: false,
        ease: 'ease',
		transition: 'scroll',
		mousewheel: true,
		arrowkey: true
	});
	pw.freeze(true);


	pw.on('before', function(i) {
        $('.cur').removeClass('cur');
        var current_page = $('.page').eq(i);
        var animation_class = ['tt-show','tt','line-anim-show'];
        if(current_page.hasClass('showed')){
            for(var i=0;i<animation_class.length;i++){
                current_page.find('.'+animation_class[i]).removeClass(animation_class[i]);
                // console.log("."+animation_class[i]);
            }
        }

	})
	pw.on('after', function(i) {
		$('.page').eq(i).addClass('cur showed');
	})
    
	
})

