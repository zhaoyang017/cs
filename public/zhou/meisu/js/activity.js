$(function(){
    var activityTime = [
        '4.28-4.29',
        '4.29-4.30',
        '4.29-5.1',
        '4.30-5.1',
        '5.1-5.3',
        '5.4-5.6',
        '5.5-5.6',
        '5.6-5.7',
        '5.11-5.13',
        '5.12-5.13',
        '5.18-5.20',
        '5.19-5.20',
        '5.20-5.21',
        '5.26-5.27',
        '5.27-5.28',
        '6.1-6.2',
        '6.2-6.3',
        '6.1-6.3',
        '6.3-6.4',
        '6.9-6.10',
        '6.10-6.11'
    ];
    var location = [
        '稚趣杭州临平店',
        '大张洛阳长申国际店',
        '沃尔玛购物广场深圳沙井店',
        '登康昆明大观店',
        '乐友北京牡丹园店',
        '红孩子富弘广场店',
        '孩子王沈阳万达沈辽路店',
        '乐友天津怡乐天地店',
        '华润万家西安三桥店',
        '孩子王洛阳万达广场店',
        '苏州唯宝母婴胜浦店',
        '红孩子广州萝岗万达店',
        '爱婴室上海浦建路店',
        '欧尚成都高新店',
        '山姆会员福州店',
        '大润发杭州富阳店',
        '家乐福北京天通苑店',
        '婴格昆明北市区店',
        '孩子王重庆江南大道店',
        '孩子王无锡京东广场店',
        '孩子王苏州繁花店',
        '红孩子无锡苏宁广场店',
        '育婴堂嘉兴海盐海滨店',
        '广州百货广州北京路店',
        '深圳港隆行布吉店',
        '孩子王南京',
        '乐友西安阳光城店',
        '婴知岛苏州绿宝店',
        '孩子王南京万达广场店',
        '孩子王青岛李沧万达店',
        '宝贝在线嵩山南路店',
        '苏果超市南京浦珠北路购物广场',
        '红孩子广州增城万达店',
        '沃尔玛购物广场厦门SM分店',
        '雅布力嘉兴一店',
        '深圳中阁娃娃大浪店',
        '丽家宝贝北京槐房万达广场店',
        '苏果超市南京金箔路购物广场店',
        '永辉超市重庆江北生活馆店',
        '好儿尚重庆美联国际店',

        '中亿成都优品道店',
        '永辉超市成都锦江城市花园店',
        '红孩子武汉经开万达广场店',
        '咿呀广州祈福店',
        '孩子王东莞厚街店',
        '孩子王合肥瑶海万达店',
        '红孩子南京徐庄店',
        '孩子王郑州二七万达店',
        '孩子王重庆万象城店',
        '福州孩子王台江万达店',
        '孩子王广州南沙万达店',
        '米氏沈阳于洪长客店',
        '好又多广州天河店',
        '深圳市新优海淘前海店',
        '爱婴室上海七莘路怡丰城店',
        '婴贝儿济南和谐广场店',
        '孩子王成都奥克斯店',
        '心聚心杭州省妇保店',
        '宁海新人类宁波总店',
        '物美北京大兴店',
        '贝贝熊长沙星沙店',
        '待定',
        '华润万家乐购系统',
        '孩子王武汉经开万达店',
        '爱婴室宁波新城银泰店'
    ];
    
    // 活动时间与地点写入数据
    var dataHtml = '';
    var locationHtml = '';
    for(var i = 0 ; i < activityTime.length;i++){
        dataHtml += '<option>'+ activityTime[i] +'</option>';
    }
    console.log(location.length)
    for(var j = 0;j < location.length;j++){
        
        locationHtml += '<option>'+ location[j] +'</option>';
    }
    $('.date select').append(dataHtml);
    $('.activity select').append(locationHtml);
})