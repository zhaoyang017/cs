
(function(){
    var loader = new PxLoader();
    var URL = window.location.href;
    var BASE_PATH = URL.substring(0, URL.lastIndexOf('/') + 1);
    var realLoadingNum = 0;
    var fileList = [
        'res/images/1.png',
        'res/images/1.png',
        'res/images/2018.png',
        'res/images/arrow.png',
        'res/images/bg_loading.gif',
        'res/images/circle.png',
        'res/images/city.gif',
        'res/images/dot.png',
        'res/images/h.png',
        'res/images/header.png',
        'res/images/light_effect.png',
        'res/images/light.png',
        'res/images/logo.png',
        'res/images/q.png',
        'res/images/text1.png',
        'res/images/text2.png',
        'res/images/y.png',
        'res/images/audio.png',
        'res/images/t.png',
        'res/images/zhong1.png',
        'res/images/yqh.png'
    ];
    for(var i = 0; i < fileList.length; i++){
        var pxImage = new PxLoaderImage(BASE_PATH + fileList[i]);
        pxImage.imageNumber = i + 1;
        loader.add(pxImage);
    }
    loader.addCompletionListener(function(){
        console.log("预加载图片："+fileList.length+"张");
    });
    loader.addProgressListener(function(e){
        var percent = Math.round( (e.completedCount / e.totalCount) * 100); //正序, 1-100
        realLoadingNum = percent;
        $('.loadNum').text(realLoadingNum + '%');
        if(realLoadingNum >= 100){
            $('.invitation').show();
            $('.loading').hide();
            init();
        }
    });
    loader.start();

    function init(){
        setTimeout(function(){
            $('.bg_loading').attr('src','res/images/bg_loading.gif');
        },500);
        
        var mySwiper = new Swiper ('.swiper-container', {
            direction:'vertical',
            on:{
                init: function(){
                    swiperAnimateCache(this); //隐藏动画元素 
                    swiperAnimate(this); //初始化完成开始动画
                }, 
                slideChangeTransitionEnd: function(){ 
                    swiperAnimate(this); //每个slide切换结束时也运行当前slide动画
                    if(this.activeIndex == 1){
                        $('.bg_loading').attr('src','');
                    }else if(this.activeIndex == 0){
                        $('.bg_loading').attr('src','res/images/bg_loading.gif');
                    }
                }
            }
        }) ;
    }

    var audio = document.getElementById('audio');
        document.addEventListener("WeixinJSBridgeReady", function () {
            audio.play();
        }, false);
        window.addEventListener('touchstart', function firstTouch(){
            audio.play();
            this.removeEventListener('touchstart', firstTouch);
        });

        var musicState = true;
        $('.audio').click(function(){
            if(musicState){
                $(this).removeClass('active');
                musicState = false;
                audio.pause();
            }else{
                musicState = true;
                $(this).addClass('active');
                audio.play();
            }
        });

        //form

    var formData = {
        username: '',   //姓名
        phone: '',      //电话
        company: '',    //公司
        job: ''    //地址
    };
    var status = true;
    function submit(){
        var regPhone = /^\d{11}$/;
        if($('.username').val() == ''){
            alert('姓名不能为空');
        }else if($('.phone').val() == ''){
            alert('电话不能为空')
        }else if(!regPhone.test( $('.phone').val())){
            alert('请输入正确的电话号码')
        }else if($('.company').val() == ''){
            alert('公司名称不能为空')
        }else if($('.technical').val() == ''){
            alert('职称不能为空')
        }else{
            formData.username = $('.username').val();
            formData.phone = $('.phone').val();
            formData.company = $('.company').val();
            formData.job = $('.technical').val();
            if(status){
                $.ajax({
                    url: 'http://api.touchworld-sh.com/api/zt/user',
                    type: 'POST',
                    data: formData,
                    success: function(){
                       alert('提交成功');
                       $('.submit').css('background-color','#888');
                       $('.submit')[0].removeEventListener('touchstart',submit)
                    },
                    error: function(){
    
                    }
                })
            }
            status = false;
        }
    }
    $('.submit')[0].addEventListener('touchstart',submit)
    
})();