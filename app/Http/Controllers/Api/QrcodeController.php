<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class QrcodeController extends Controller
{
    public function index()
    {
//        return view('test');
    }
    public function store(Request $request)
    {
        //存储拍照图片返回路径
        $this->validate($request, [
            'photo' => 'required|image|max:10000',
            'program' => 'max:15',
        ]);
        $path = Storage::disk('public')->putFile('photos', new File($request->photo));
        //原因未知，env取不到值
        // return response()->json(['qrcode' => env('APP_URL').'/storage/'.$path]);
        return response()->json(['qrcode' => 'http://cs.touchworld-sh.com/storage/'.$path]);
    }
}

